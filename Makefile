CC=c++
CFLAGS=-c -Wall
LDFLAGS=-lGL -lGLU -lX11 -lXext -lXrender -lXfixes
SRC=rain.o window.o
CPP=rain.cpp window.cpp

all: xrain

xrain: $(SRC)
	$(CC) $(SRC) $(LDFLAGS) -o Xrain

$(SRC): $(CPP)
	$(CC) $(CFLAGS) $(CPP)

clean:
	rm -rf *.o Xrain

install:
	install Xrain /usr/local/bin

uninstall:
	rm -rf /usr/local/bin/Xrain