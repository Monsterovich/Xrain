Run 'make && sudo make install'.

```
$ Xrain

$ Xrain -a -13 -s 4 -o -20 -i 140 -p 2048
```

Usage: Xrain [-s rain_speed (def:5.0)] [-p num_particles (def:255)] [-a angle (def 0.0)] [-x x_size (def:0.04)] [-y y_size (def:3)] [-o spawn_left_x (def:0)] [-i spawn_right_x (def:100.0)]

https://youtu.be/ZFZlgHjzP3Q