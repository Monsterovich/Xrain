
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/keysym.h>

#include <X11/extensions/shape.h>
#include <X11/extensions/Xfixes.h>

#include "rain.h"


int main(int argc, char* argv[])
{
	if(!parse_args(argc, argv))
		return 1;

	init_effect();

	Display *display = XOpenDisplay(0);
	GC gc;
	(void)gc;
	const char *xserver = getenv("DISPLAY");

	if (display == 0)
	{
		printf("Could not establish a connection to X-server '%s'\n", xserver) ;
		return 1;
	}

	XWindowAttributes ra;
	XGetWindowAttributes(display, DefaultRootWindow(display), &ra);
	XVisualInfo visualinfo ;
	XMatchVisualInfo(display, DefaultScreen(display), 32, TrueColor, &visualinfo);

	// create window
	Window win;
	XSetWindowAttributes attr;
	attr.colormap   = XCreateColormap( display, DefaultRootWindow(display), visualinfo.visual, AllocNone) ;
	attr.event_mask = ExposureMask;
	attr.background_pixmap = None;
	attr.border_pixel = 0;
	win = XCreateWindow(display, DefaultRootWindow(display), 0, 0, 
						ra.width, ra.height, 0,visualinfo.depth, InputOutput, visualinfo.visual, 
						CWColormap|CWEventMask|CWBackPixmap|CWBorderPixel, &attr) ;
	gc = XCreateGC(display, win, 0, 0);

	XStoreName( display, win, "Xrain" );

	// say window manager which position we would prefer
	XSizeHints sizehints;
	sizehints.flags = PPosition | PSize;
	sizehints.x = sizehints.y = 0;
	sizehints.width = ra.width; sizehints.height = ra.height;
	XSetWMNormalHints(display, win, &sizehints);
	// Switch On >> If user pressed close key let window manager only send notification >>
	Atom wm_delete_window = XInternAtom( display, "WM_DELETE_WINDOW", 0) ;
	XSetWMProtocols(display, win, &wm_delete_window, 1);

	// properties
	Atom window_state = XInternAtom(display, "_NET_WM_STATE", 0);
	Atom value2 = XInternAtom(display, "_NET_WM_STATE_BELOW", 0);
	XChangeProperty (display, win, window_state, XA_ATOM, 32, PropModeAppend, (unsigned char *) &value2, 1);

	Atom window_type2 = XInternAtom(display, "_NET_WM_WINDOW_TYPE", 0);
	Atom value3 = XInternAtom(display, "_NET_WM_WINDOW_TYPE_DOCK", 0);
	XChangeProperty (display, win, window_type2, XA_ATOM, 32, PropModeAppend, (unsigned char *) &value3, 1);

	Atom allowed_actions = XInternAtom(display, "_NET_WM_ALLOWED_ACTIONS", 0);
	Atom value4 = XInternAtom(display, "_NET_WM_ACTION_MOVE", 0);
	XChangeProperty (display, win, allowed_actions, XA_ATOM, 32, PropModeAppend, (unsigned char *) &value4, 0);

	Atom allowed_actions2 = XInternAtom(display, "_NET_WM_ALLOWED_ACTIONS", 0);
	Atom value5 = XInternAtom(display, "_NET_WM_ACTION_RESIZE", 0);
	XChangeProperty (display, win, allowed_actions2, XA_ATOM, 32, PropModeAppend, (unsigned char *) &value5, 0);

	// create OpenGL context
	GLXContext glcontext = glXCreateContext( display, &visualinfo, 0, True ) ;
	if (!glcontext)
	{
		printf("X11 server '%s' does not support OpenGL\n", xserver ) ;
		return 1;
	}
	glXMakeCurrent(display, win, glcontext) ;

	// this should be called before or pass-through won't work
	XMapWindow(display, win) ;
	XFlush(display);

	// make window pass-through
	XRectangle rect;
	XserverRegion region = XFixesCreateRegion(display, &rect, 1);
	XFixesSetWindowShapeRegion(display, win, ShapeInput, 0, 0, region);
	XFixesSetWindowShapeRegion (display, win, ShapeBounding, 0, 0, 0);
	XFixesDestroyRegion(display, region);

	init_scene((float)ra.width, (float)ra.height);

	bool isUserWantsWindowToClose = false;
	while( !isUserWantsWindowToClose )
	{
		while( XPending(display) > 0 )
		{
			XEvent event ;
			XNextEvent( display, &event) ;

			switch(event.type)
			{
				case ClientMessage:
					if (event.xclient.message_type == XInternAtom( display, "WM_PROTOCOLS", 1) && (Atom)event.xclient.data.l[0]  == XInternAtom( display, "WM_DELETE_WINDOW", 1))
						isUserWantsWindowToClose = true;
					break;
			}
		}

		draw_scene();
		glXSwapBuffers(display, win) ;
		//glXWaitGL();

		process_scene();
	}

	XDestroyWindow(display, win); win = 0;
	XCloseDisplay(display); display = 0 ;
	clear_scene();

	return 0;
}
